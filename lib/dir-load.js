'use strict'

const path = require('path');
const require_directory = require('require-dir');
const fs = require('fs');
const utilJson = require('util-json');

module.exports = function(app, currentPath, option,name){
  var dir = utilJson.findObjectByKey('dir',option);
  if(dir){
    console.log("%s配置了%s", path.basename(currentPath),name);
    let optionPath = path.join(currentPath,dir);
    let optionDirExists = fs.existsSync(optionPath);
    if(optionDirExists){
      console.log("%s %s文件夹存在", path.basename(currentPath),name);
      //尝试require 文件夹，有可能存在的异常:1.options中有未找到的模块
      try {
        return require_directory(optionPath, { recurse: true });
      } catch (err) {
        throw err;
      }
    }else{
      console.error("%s %s文件夹不存在", path.basename(currentPath),name);
    }
  }else{
    console.log("%s 没有找到%s配置", path.basename(currentPath),name);
  }
}
